package de.sidion.saga.confirmationservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ConfirmationKafkaConsumer {

    private final ConfirmationService confirmationService;

    public ConfirmationKafkaConsumer(ConfirmationService confirmationService) {
        this.confirmationService = confirmationService;
    }

    @KafkaListener(topics = "bookings-orchestration", containerFactory = "bookingEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final BookingEvent bookingEvent) {
        log.info("incomming bookingEvent: {}", bookingEvent);
        confirmationService.createConfirmationFromBooking(bookingEvent);
    }

    @KafkaListener(topics = "confirmation-orchestration", containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent: {}", sagaEvent);
        if (sagaEvent.getEventName().equals(OrchestrationEventName.APPLY_CONFIRMATION)) {
            confirmationService.sendConfirmationEmail(sagaEvent.getBookingId());
        }
    }


}
