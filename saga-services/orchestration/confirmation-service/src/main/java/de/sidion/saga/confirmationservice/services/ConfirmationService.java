package de.sidion.saga.confirmationservice.services;

import de.sidion.saga.confirmationservice.entities.ConfirmationEntitiy;
import de.sidion.saga.confirmationservice.persistence.ConfirmationRepository;
import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.ReplyEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class ConfirmationService {

    private final ConfirmationRepository confirmationRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public ConfirmationService(ConfirmationRepository confirmationRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.confirmationRepository = confirmationRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createConfirmationFromBooking(final BookingEvent bookingEvent) {
        final ConfirmationEntitiy confirmationEntitiy = ConfirmationEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .customerName(bookingEvent.getCustomerEvent().getName())
                .email(bookingEvent.getCustomerEvent().getEmail())
                .bookingStatus("PENDING")
                .build();

        confirmationRepository.save(confirmationEntitiy);

    }


    public void sendConfirmationEmail(final Integer bookingId) {
        final ConfirmationEntitiy confirmationEntitiy = confirmationRepository.findByBookingId(bookingId);
        final ReplyEvent.ReplyEventBuilder builder = ReplyEvent.builder();
        builder.bookingId(bookingId);
        builder.eventName(OrchestrationEventName.CONFIRMATION_APPLIED);

        confirmationEntitiy.setBookingStatus("COMPLETED");
        confirmationRepository.save(confirmationEntitiy);


        sendSagaEvent(builder.build());
    }

    private void sendSagaEvent(ReplyEvent replyEvent) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send("saga-reply-orchestration", String.valueOf(replyEvent.getBookingId()), replyEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending reply {}", replyEvent);
            }
        });
    }

}
