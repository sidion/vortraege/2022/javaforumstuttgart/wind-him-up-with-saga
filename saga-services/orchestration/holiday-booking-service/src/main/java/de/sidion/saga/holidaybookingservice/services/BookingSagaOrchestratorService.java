package de.sidion.saga.holidaybookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.ReplyEvent;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.holidaybookingservice.entities.BookingEntitiy;
import de.sidion.saga.holidaybookingservice.persistence.BookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class BookingSagaOrchestratorService {

    private final BookingRepository bookingRepository;
    private final BookingEventToBookingEntityMapper bookingEventToBookingEntityMapper;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public BookingSagaOrchestratorService(BookingRepository bookingRepository, BookingEventToBookingEntityMapper bookingEventToBookingEntityMapper, KafkaTemplate<String, Object> kafkaTemplate) {
        this.bookingRepository = bookingRepository;
        this.bookingEventToBookingEntityMapper = bookingEventToBookingEntityMapper;
        this.kafkaTemplate = kafkaTemplate;
    }

    @KafkaListener(topics = "saga-reply-orchestration", containerFactory = "sagaReplyEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final ReplyEvent replyEvent) {
        log.info("incomming replyEvent: {}", replyEvent);
        final OrchestrationEventName replyEventName = replyEvent.getEventName();
        final Integer bookingId = replyEvent.getBookingId();
        final BookingEntitiy bookingEntitiy = bookingRepository.findById(bookingId).get();
        final BookingEvent bookingEvent = bookingEventToBookingEntityMapper.createEventFromEntity(bookingEntitiy);
        if (replyEventName.equals(OrchestrationEventName.CARD_VERIFIED)) {
            applyFlightBooking(bookingId);
        }
        if (replyEventName.equals(OrchestrationEventName.FLIGHT_BOOKING_APPLIED)) {
            applyHotelBooking(bookingId);
        }
        if (replyEventName.equals(OrchestrationEventName.HOTEL_BOOKING_APPLIED)) {
            debitCard(bookingId);
        }
        if (replyEventName.equals(OrchestrationEventName.DEBIT_APPLIED)) {
            applyConfirmation(bookingId);
        }
        if (replyEventName.equals(OrchestrationEventName.CONFIRMATION_APPLIED)) {
            handleFinishBooking(bookingId);
        }

        if (replyEventName.equals(OrchestrationEventName.CARD_VERIFICATION_FAILED)
                || replyEventName.equals(OrchestrationEventName.FLIGHT_BOOKING_FAILED)
                || replyEventName.equals(OrchestrationEventName.HOTEL_BOOKING_FAILED)
                || replyEventName.equals(OrchestrationEventName.DEBIT_CARD_FAILED)) {
            handleRejectBooking(bookingId, replyEventName);
        }

    }

    private void handleFinishBooking(final Integer bookingId) {
        final BookingEntitiy bookingEntitiy = bookingRepository.findById(bookingId).get();
        bookingEntitiy.setBookingStatus("COMPLETED");

        bookingRepository.save(bookingEntitiy);
    }

    private void handleRejectBooking(final Integer bookingId, OrchestrationEventName eventName) {
        log.info("reject Booking with id {} because of {}", bookingId, eventName);

        final BookingEntitiy bookingEntitiy = bookingRepository.findById(bookingId).get();
        bookingEntitiy.setBookingStatus("CANCELED");

        bookingRepository.save(bookingEntitiy);

        rejectFlightBooking(bookingId);
        rejectHotelBooking(bookingId);
    }

    public void sendBooking(BookingEvent bookingEvent) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send("bookings-orchestration", String.valueOf(bookingEvent.getBookingId()), bookingEvent);

        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("send bookingEvent to topic: {}", "bookings-orchestration");
            }
        });

    }

    public void verifyCard(final Integer bookingId) {
        final SagaEvent sagaEvent = SagaEvent.builder().bookingId(bookingId).eventName(OrchestrationEventName.VERIFY_CARD).build();
        sendSagaEvent(sagaEvent, "payments-orchestration");
    }

    private void debitCard(final Integer bookingId) {
        final SagaEvent sagaEvent = SagaEvent.builder().bookingId(bookingId).eventName(OrchestrationEventName.DEBIT_CARD).build();
        sendSagaEvent(sagaEvent, "payments-orchestration");
    }

    private void applyFlightBooking(final Integer bookingId) {
        final SagaEvent sagaEvent = SagaEvent.builder().bookingId(bookingId).eventName(OrchestrationEventName.APPLY_FLIGHT_BOOKING).build();
        sendSagaEvent(sagaEvent, "flight-bookings-orchestration");
    }

    private void applyHotelBooking(final Integer bookingId) {
        final SagaEvent sagaEvent = SagaEvent.builder().bookingId(bookingId).eventName(OrchestrationEventName.APPLY_HOTEL_BOOKING).build();
        sendSagaEvent(sagaEvent, "hotel-bookings-orchestration");
    }

    private void applyConfirmation(final Integer bookingId) {
        final SagaEvent sagaEvent = SagaEvent.builder().bookingId(bookingId).eventName(OrchestrationEventName.APPLY_CONFIRMATION).build();
        sendSagaEvent(sagaEvent, "confirmation-orchestration");
    }

    private void rejectFlightBooking(final Integer bookingId) {
        final SagaEvent sagaEvent = SagaEvent.builder().bookingId(bookingId).eventName(OrchestrationEventName.REJECT_FLIGHT_BOOKING).build();
        sendSagaEvent(sagaEvent, "flight-bookings-orchestration");
    }

    private void rejectHotelBooking(final Integer bookingId) {
        final SagaEvent sagaEvent = SagaEvent.builder().bookingId(bookingId).eventName(OrchestrationEventName.REJECT_HOTEL_BOOKING).build();
        sendSagaEvent(sagaEvent, "hotel-bookings-orchestration");
    }

    private void sendSagaEvent(SagaEvent sagaEvent, String topic) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(topic, String.valueOf(sagaEvent.getBookingId()), sagaEvent);

        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("send sagaEvent with status {} to topic: {}", sagaEvent.getEventName(), topic);
            }
        });
    }


}
