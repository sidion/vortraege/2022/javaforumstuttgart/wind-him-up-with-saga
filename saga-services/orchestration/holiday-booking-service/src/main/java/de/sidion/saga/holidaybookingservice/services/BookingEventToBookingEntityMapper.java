package de.sidion.saga.holidaybookingservice.services;

import de.sidion.saga.events.*;
import de.sidion.saga.holidaybookingservice.entities.BookingEntitiy;
import org.springframework.stereotype.Component;

@Component
public class BookingEventToBookingEntityMapper {

    public BookingEntitiy createEntityFromEvent(final BookingEvent bookingEvent, final String status) {

        return BookingEntitiy.builder()
                .bookingStatus(status)
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .customerName(bookingEvent.getCustomerEvent().getName())
                .email(bookingEvent.getCustomerEvent().getEmail())
                .outgoingFlightId(bookingEvent.getFlightEvent().getOutgoingFlightId())
                .outgoingFlightDate(bookingEvent.getFlightEvent().getOutgoingFlightDate())
                .returnFlightId(bookingEvent.getFlightEvent().getReturnFlightId())
                .returnFlightDate(bookingEvent.getFlightEvent().getReturnFlightDate())
                .hotelId(bookingEvent.getHotelEvent().getHotelId())
                .hotelFromDate(bookingEvent.getHotelEvent().getFromDate())
                .hotelToDate(bookingEvent.getHotelEvent().getToDate())
                .price(bookingEvent.getPaymentEvent().getPrice())
                .cardNumber(bookingEvent.getPaymentEvent().getCardNumber())
                .build();

    }


    public BookingEvent createEventFromEntity(BookingEntitiy bookingEntitiy) {
        final CustomerEvent customerEvent = CustomerEvent.builder()
                .customerId(bookingEntitiy.getCustomerId())
                .name(bookingEntitiy.getCustomerName())
                .email(bookingEntitiy.getEmail())
                .build();

        final FlightEvent flightEvent = FlightEvent.builder()
                .outgoingFlightId(bookingEntitiy.getOutgoingFlightId())
                .outgoingFlightDate(bookingEntitiy.getOutgoingFlightDate())
                .returnFlightId(bookingEntitiy.getReturnFlightId())
                .returnFlightDate(bookingEntitiy.getReturnFlightDate())
                .build();

        final HotelEvent hotelEvent = HotelEvent.builder()
                .hotelId(bookingEntitiy.getHotelId())
                .fromDate(bookingEntitiy.getHotelFromDate())
                .toDate(bookingEntitiy.getHotelToDate())
                .build();

        final PaymentEvent paymentEvent = PaymentEvent.builder()
                .cardNumber(bookingEntitiy.getCardNumber())
                .price(bookingEntitiy.getPrice())
                .build();

        final BookingEvent bookingEvent = BookingEvent.builder()
                .bookingId(bookingEntitiy.getBookingId())
                .flightEvent(flightEvent)
                .hotelEvent(hotelEvent)
                .customerEvent(customerEvent)
                .paymentEvent(paymentEvent)
                .build();

        return bookingEvent;
    }
}
