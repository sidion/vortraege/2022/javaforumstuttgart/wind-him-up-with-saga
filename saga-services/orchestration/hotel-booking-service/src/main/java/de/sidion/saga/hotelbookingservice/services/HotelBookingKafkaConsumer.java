package de.sidion.saga.hotelbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HotelBookingKafkaConsumer {

    private final HotelBookingService hotelBookingService;

    public HotelBookingKafkaConsumer(HotelBookingService hotelBookingService) {
        this.hotelBookingService = hotelBookingService;
    }

    @KafkaListener(topics = "bookings-orchestration", containerFactory = "bookingEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final BookingEvent bookingEvent) {
        log.info("incomming bookingEvent: {}", bookingEvent);
        hotelBookingService.createHotelFromBooking(bookingEvent);
    }

    @KafkaListener(topics = "hotel-bookings-orchestration", containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent: {}", sagaEvent);
        if (sagaEvent.getEventName().equals(OrchestrationEventName.APPLY_HOTEL_BOOKING)) {
            hotelBookingService.handleUpdate(sagaEvent.getBookingId());
        } else if (sagaEvent.getEventName().equals(OrchestrationEventName.REJECT_HOTEL_BOOKING)) {
            log.warn("cancel booking with bookingid {}", sagaEvent.getBookingId());
            hotelBookingService.rejectBooking(sagaEvent.getBookingId());
        }

    }


}
