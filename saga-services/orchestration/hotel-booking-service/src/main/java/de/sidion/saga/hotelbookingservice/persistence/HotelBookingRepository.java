package de.sidion.saga.hotelbookingservice.persistence;

import de.sidion.saga.hotelbookingservice.entities.HotelBookingEntitiy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HotelBookingRepository extends JpaRepository<HotelBookingEntitiy, Integer> {

    HotelBookingEntitiy findByBookingIdAndBookingStatus(Integer bookingId, String bookingStatus);
}
