package de.sidion.saga.hotelbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.ReplyEvent;
import de.sidion.saga.hotelbookingservice.entities.HotelBookingEntitiy;
import de.sidion.saga.hotelbookingservice.persistence.HotelBookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import javax.persistence.NonUniqueResultException;

@Service
@Slf4j
public class HotelBookingService {

    private final HotelBookingRepository hotelBookingRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;


    public HotelBookingService(HotelBookingRepository hotelBookingRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.hotelBookingRepository = hotelBookingRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createHotelFromBooking(final BookingEvent bookingEvent) {
        final HotelBookingEntitiy hotelBookingEntitiy = HotelBookingEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .hotelId(bookingEvent.getHotelEvent().getHotelId())
                .hotelFromDate(bookingEvent.getHotelEvent().getFromDate())
                .hotelToDate(bookingEvent.getHotelEvent().getToDate())
                .bookingStatus("PENDING")
                .build();

        hotelBookingRepository.save(hotelBookingEntitiy);

    }

    public void handleUpdate(final Integer bookingId) {
        final HotelBookingEntitiy hotelBookingEntity = hotelBookingRepository.findByBookingIdAndBookingStatus(bookingId, "PENDING");
        final ReplyEvent.ReplyEventBuilder builder = ReplyEvent.builder();
        builder.bookingId(bookingId);

        if (isValidHotelBooking(hotelBookingEntity)) {
            builder.eventName(OrchestrationEventName.HOTEL_BOOKING_APPLIED);
            hotelBookingEntity.setBookingStatus("COMPLETED");
            hotelBookingRepository.save(hotelBookingEntity);
        } else {
            builder.eventName(OrchestrationEventName.HOTEL_BOOKING_FAILED);
        }

        sendSagaEvent(builder.build());
    }

    public void rejectBooking(final Integer bookingId) {
        final HotelBookingEntitiy hotelBookingEntitiyToDelete = hotelBookingRepository.findByBookingIdAndBookingStatus(bookingId, "PENDING");
        if (hotelBookingEntitiyToDelete != null) {
            log.info("DELETE hotelBookingEntitiy: {}", hotelBookingEntitiyToDelete);
            hotelBookingRepository.delete(hotelBookingEntitiyToDelete);
        }

    }

    private void sendSagaEvent(ReplyEvent replyEvent) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send("saga-reply-orchestration", String.valueOf(replyEvent.getBookingId()), replyEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending reply {}", replyEvent);
            }
        });
    }


    private boolean isValidHotelBooking(final HotelBookingEntitiy hotelBookingEntity) {
        // only RIU hotels are valid
        return hotelBookingEntity.getHotelId().startsWith("RIU");
    }
}
