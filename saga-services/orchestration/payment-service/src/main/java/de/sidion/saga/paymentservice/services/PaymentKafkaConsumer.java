package de.sidion.saga.paymentservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PaymentKafkaConsumer {

    private final PaymentService paymentService;

    public PaymentKafkaConsumer(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @KafkaListener(topics = "bookings-orchestration", containerFactory = "bookingEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final BookingEvent bookingEvent) {
        log.info("incomming bookingEvent: {}", bookingEvent);
        paymentService.createPaymentFromBooking(bookingEvent);
    }

    @KafkaListener(topics = "payments-orchestration", containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToPayments(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent: {}", sagaEvent);
        if (sagaEvent.getEventName().equals(OrchestrationEventName.VERIFY_CARD)) {
            paymentService.verifyCard(sagaEvent.getBookingId());
        } else if (sagaEvent.getEventName().equals(OrchestrationEventName.DEBIT_CARD)) {
            log.warn("cancel booking with id {}", sagaEvent.getBookingId());
            paymentService.debitCard(sagaEvent.getBookingId());
        }
    }

}
