package de.sidion.saga.paymentservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.ReplyEvent;
import de.sidion.saga.paymentservice.entities.PaymentEntitiy;
import de.sidion.saga.paymentservice.persistence.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public PaymentService(PaymentRepository paymentRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.paymentRepository = paymentRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createPaymentFromBooking(final BookingEvent bookingEvent) {
        final PaymentEntitiy carRentEntitiy = PaymentEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .price(bookingEvent.getPaymentEvent().getPrice())
                .cardNumber(bookingEvent.getPaymentEvent().getCardNumber())
                .bookingStatus("PENDING")
                .build();

        paymentRepository.save(carRentEntitiy);

    }

    public void verifyCard(final Integer bookingId) {
        final PaymentEntitiy paymentEntitiy = paymentRepository.findByBookingId(bookingId);
        final ReplyEvent.ReplyEventBuilder builder = ReplyEvent.builder();
        builder.bookingId(bookingId);

        if (isValidCardNumber(paymentEntitiy)) {
            builder.eventName(OrchestrationEventName.CARD_VERIFIED);
            paymentEntitiy.setBookingStatus("VERIFIED");
            paymentRepository.save(paymentEntitiy);
        } else {
            builder.eventName(OrchestrationEventName.CARD_VERIFICATION_FAILED);
            rejectBooking(bookingId);
        }

        sendSagaEvent(builder.build());
    }

    public void debitCard(final Integer bookingId) {
        final PaymentEntitiy paymentEntitiy = paymentRepository.findByBookingId(bookingId);
        final ReplyEvent.ReplyEventBuilder builder = ReplyEvent.builder();
        builder.bookingId(bookingId);

        if (isDebitSuccessful(paymentEntitiy)) {
            builder.eventName(OrchestrationEventName.DEBIT_APPLIED);
            paymentEntitiy.setBookingStatus("COMPLETED");
            paymentRepository.save(paymentEntitiy);
        } else {
            builder.eventName(OrchestrationEventName.DEBIT_CARD_FAILED);
            rejectBooking(bookingId);
        }

        sendSagaEvent(builder.build());

    }

    public void rejectBooking(final Integer bookingId) {
        final PaymentEntitiy paymentEntitiyToDelete = paymentRepository.findByBookingId(bookingId);
        if (paymentEntitiyToDelete != null) {
            log.info("DELETE paymentEntitiy: {}", paymentEntitiyToDelete);
            paymentRepository.delete(paymentEntitiyToDelete);
        }
    }

    private void sendSagaEvent(ReplyEvent replyEvent) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send("saga-reply-orchestration", String.valueOf(replyEvent.getBookingId()), replyEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending reply {}", replyEvent);
            }
        });
    }

    private boolean isValidCardNumber(PaymentEntitiy paymentEntitiy) {
        final String cardNumber = paymentEntitiy.getCardNumber();
        if (cardNumber.length() != 15) {
            return false;
        }
        if (!cardNumber.startsWith("37") || cardNumber.startsWith("34")) {
            return false;
        }
        return true;
    }

    private boolean isDebitSuccessful(PaymentEntitiy paymentEntitiy) {
        return !(paymentEntitiy.getCardNumber().equals("371234567891008") && paymentEntitiy.getPrice() > 1500);
    }

}
