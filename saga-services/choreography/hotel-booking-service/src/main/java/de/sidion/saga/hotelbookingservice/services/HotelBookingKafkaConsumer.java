package de.sidion.saga.hotelbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HotelBookingKafkaConsumer {

    private final HotelBookingService hotelBookingService;

    public HotelBookingKafkaConsumer(HotelBookingService hotelBookingService) {
        this.hotelBookingService = hotelBookingService;
    }

    @KafkaListener(topics = TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, containerFactory = "bookingEventKafkaListenerContainerBatchFactory")
    public void listenToBookingsTopic(@Payload final BookingEvent bookingEvent) {
        log.info("incomming bookingEvent from {}: {}", TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, bookingEvent);
        hotelBookingService.createNewHotelBooking(bookingEvent);
    }

    @KafkaListener(topics = TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToHotelBookingsTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.APPLY_HOTEL_BOOKING)) {
            hotelBookingService.handleUpdate(sagaEvent.getBookingId());
        } else if (sagaEvent.getEventName().equals(ChoreographyEventName.HOTEL_BOOKING_FAILED)) {
            hotelBookingService.rejectBooking(sagaEvent.getBookingId());
        }

    }


    @KafkaListener(topics = TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToPaymentTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.DEBIT_CARD_FAILED)) {
            hotelBookingService.rejectBooking(sagaEvent.getBookingId());
        }
    }


}
