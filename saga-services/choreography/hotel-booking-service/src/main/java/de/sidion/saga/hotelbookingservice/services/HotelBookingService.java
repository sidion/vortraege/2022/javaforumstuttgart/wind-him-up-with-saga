package de.sidion.saga.hotelbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import de.sidion.saga.hotelbookingservice.entities.HotelBookingEntitiy;
import de.sidion.saga.hotelbookingservice.persistence.HotelBookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class HotelBookingService {

    private final HotelBookingRepository hotelBookingRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;


    public HotelBookingService(HotelBookingRepository hotelBookingRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.hotelBookingRepository = hotelBookingRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createNewHotelBooking(final BookingEvent bookingEvent) {

        final HotelBookingEntitiy hotelBookingEntitiy = HotelBookingEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .hotelId(bookingEvent.getHotelEvent().getHotelId())
                .hotelFromDate(bookingEvent.getHotelEvent().getFromDate())
                .hotelToDate(bookingEvent.getHotelEvent().getToDate())
                .bookingStatus("PENDING")
                .build();

        hotelBookingRepository.save(hotelBookingEntitiy);


    }

    public void handleUpdate(final Integer bookingId) {
        final HotelBookingEntitiy hotelBookingEntitiy = hotelBookingRepository.findByBookingId(bookingId);
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(bookingId);
        if (isValidHotelBooking(hotelBookingEntitiy)) {
            builder.eventName(ChoreographyEventName.DEBIT_APPLIED);
            hotelBookingEntitiy.setBookingStatus("COMPLETED");
            hotelBookingRepository.save(hotelBookingEntitiy);
            sendSagaEvent(builder.build(), TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY);
        } else {
            builder.eventName(ChoreographyEventName.HOTEL_BOOKING_FAILED);
            final SagaEvent sagaEvent = builder.build();
            sendSagaEvent(sagaEvent, TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY);
            sendSagaEvent(sagaEvent, TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY);
        }


    }

    private void sendSagaEvent(SagaEvent sagaEvent, String topic) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(topic, String.valueOf(sagaEvent.getBookingId()), sagaEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending sagaEvent {} to topic {}", sagaEvent, topic);
            }
        });
    }


    public void rejectBooking(Integer bookingId) {
        final HotelBookingEntitiy hotelBookingEntitiy = hotelBookingRepository.findByBookingId(bookingId);
        if (hotelBookingEntitiy != null) {
            hotelBookingRepository.delete(hotelBookingEntitiy);
        }
    }


    private boolean isValidHotelBooking(final HotelBookingEntitiy hotelBookingEntitiy) {
        // only RIU hotels are valid
        return hotelBookingEntitiy.getHotelId().startsWith("RIU");
    }
}
