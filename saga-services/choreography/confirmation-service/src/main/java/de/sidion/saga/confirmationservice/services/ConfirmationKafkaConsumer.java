package de.sidion.saga.confirmationservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ConfirmationKafkaConsumer {

    private final ConfirmationService confirmationService;

    public ConfirmationKafkaConsumer(ConfirmationService confirmationService) {
        this.confirmationService = confirmationService;
    }

    @KafkaListener(topics = TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, containerFactory = "bookingEventKafkaListenerContainerBatchFactory")
    public void listenToBookingsTopic(@Payload final BookingEvent bookingEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, bookingEvent);
        confirmationService.createNewConfirmation(bookingEvent);
    }

    @KafkaListener(topics = TopicNames.TOPIC_CONFIRMATION_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_CONFIRMATION_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.APPLY_CONFIRMATION)) {
            confirmationService.handleUpdate(sagaEvent.getBookingId());
        }
    }


}
