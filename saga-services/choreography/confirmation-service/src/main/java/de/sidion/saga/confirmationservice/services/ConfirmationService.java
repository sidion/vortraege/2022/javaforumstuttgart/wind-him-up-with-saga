package de.sidion.saga.confirmationservice.services;

import de.sidion.saga.confirmationservice.entities.ConfirmationEntitiy;
import de.sidion.saga.confirmationservice.persistence.ConfirmationRepository;
import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class ConfirmationService {

    private final ConfirmationRepository confirmationRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public ConfirmationService(ConfirmationRepository confirmationRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.confirmationRepository = confirmationRepository;
        this.kafkaTemplate = kafkaTemplate;
    }


    public void createNewConfirmation(final BookingEvent bookingEvent) {
        final ConfirmationEntitiy confirmationEntitiy = ConfirmationEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .customerName(bookingEvent.getCustomerEvent().getName())
                .email(bookingEvent.getCustomerEvent().getEmail())
                .bookingStatus("PENDING")
                .build();

        confirmationRepository.save(confirmationEntitiy);


    }

    public void handleUpdate(final Integer bookingId) {
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(bookingId);
        builder.eventName(ChoreographyEventName.CONFIRMATION_APPLIED);
        final ConfirmationEntitiy confirmationEntitiy = confirmationRepository.findByBookingId(bookingId);
        confirmationEntitiy.setBookingStatus("COMPLETED");
        confirmationRepository.save(confirmationEntitiy);

        sendSagaEvent(builder.build());

    }

    private void sendSagaEvent(SagaEvent sagaEvent) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(TopicNames.TOPIC_CONFIRMATION_CHOREOGRAPHY, String.valueOf(sagaEvent.getBookingId()), sagaEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending sagaEvent {} to topic {}", sagaEvent, TopicNames.TOPIC_CONFIRMATION_CHOREOGRAPHY);
            }
        });
    }


    public void rejectBooking(Integer bookingId) {
        final ConfirmationEntitiy confirmationEntitiy = confirmationRepository.findByBookingId(bookingId);
        if (confirmationEntitiy != null) {
            confirmationRepository.delete(confirmationEntitiy);
        }
    }


}
