package de.sidion.saga.confirmationservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "confirmation")
@Entity
public class ConfirmationEntitiy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer confirmationId;
    private Integer bookingId;
    private String bookingStatus;
    private Integer customerId;
    private String customerName;
    private String email;

}
