package de.sidion.saga.paymentservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import de.sidion.saga.paymentservice.entities.PaymentEntitiy;
import de.sidion.saga.paymentservice.persistence.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public PaymentService(PaymentRepository paymentRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.paymentRepository = paymentRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createPayment(final BookingEvent bookingEvent) {
        final PaymentEntitiy carRentEntitiy = PaymentEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .price(bookingEvent.getPaymentEvent().getPrice())
                .cardNumber(bookingEvent.getPaymentEvent().getCardNumber())
                .bookingStatus("PENDING")
                .build();

        paymentRepository.save(carRentEntitiy);

    }

    public void verifyCard(final Integer bookingId) {
        final PaymentEntitiy paymentEntitiy = paymentRepository.findByBookingId(bookingId);
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(bookingId);
        if (isValidCardNumber(paymentEntitiy)) {
            builder.eventName(ChoreographyEventName.APPLY_FLIGHT_BOOKING);
            paymentEntitiy.setBookingStatus("VERIFIED");
            paymentRepository.save(paymentEntitiy);
            sendSagaEvent(builder.build(), TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY);
        } else {
            builder.eventName(ChoreographyEventName.CARD_VERIFICATION_FAILED);
            sendSagaEvent(builder.build(), TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY);
        }
    }

    public void debitCard(final Integer bookingId) {
        final PaymentEntitiy paymentEntitiy = paymentRepository.findByBookingId(bookingId);
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(bookingId);
        if (isDebitSuccessful(paymentEntitiy)) {
            builder.eventName(ChoreographyEventName.APPLY_CONFIRMATION);
            paymentEntitiy.setBookingStatus("COMPLETED");
            paymentRepository.save(paymentEntitiy);
            sendSagaEvent(builder.build(), TopicNames.TOPIC_CONFIRMATION_CHOREOGRAPHY);
        } else {
            builder.eventName(ChoreographyEventName.DEBIT_CARD_FAILED);
            final SagaEvent sagaEvent = builder.build();
            sendSagaEvent(sagaEvent, TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY);
            sendSagaEvent(sagaEvent, TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY);
            sendSagaEvent(sagaEvent, TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY);
        }
    }

    private void sendSagaEvent(SagaEvent sagaEvent, String topic) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(topic, String.valueOf(sagaEvent.getBookingId()), sagaEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending sagaEvent {} to topic {}", sagaEvent, topic);
            }
        });
    }


    private boolean isValidCardNumber(final PaymentEntitiy paymentEntitiy) {
        final String cardNumber = paymentEntitiy.getCardNumber();
        if (cardNumber.length() != 15) {
            return false;
        }
        if (!cardNumber.startsWith("37") || cardNumber.startsWith("34")) {
            return false;
        }
        return true;
    }

    private boolean isDebitSuccessful(PaymentEntitiy paymentEntitiy) {
        return paymentEntitiy.getPrice() < 1500;
    }


    public void rejectBooking(Integer bookingId) {
        final PaymentEntitiy paymentEntitiyToDelete = paymentRepository.findByBookingId(bookingId);
        if (paymentEntitiyToDelete != null) {
            log.info("DELETE paymentEntitiy: {}", paymentEntitiyToDelete);
            paymentRepository.delete(paymentEntitiyToDelete);
        }
    }
}
