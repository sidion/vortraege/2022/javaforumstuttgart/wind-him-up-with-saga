package de.sidion.saga.paymentservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PaymentKafkaConsumer {

    private final PaymentService paymentService;

    public PaymentKafkaConsumer(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @KafkaListener(topics = TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, containerFactory = "bookingEventKafkaListenerContainerBatchFactory")
    public void listenToBookingsTopic(@Payload final BookingEvent bookingEvent) {
        log.info("incomming bookingEvent from {}: {}", TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, bookingEvent);
        paymentService.createPayment(bookingEvent);
    }

    @KafkaListener(topics = TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToPaymentTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.VERIFY_CARD)) {
            paymentService.verifyCard(sagaEvent.getBookingId());
        } else if (sagaEvent.getEventName().equals(ChoreographyEventName.DEBIT_APPLIED)) {
            paymentService.debitCard(sagaEvent.getBookingId());
        } else if (sagaEvent.getEventName().equals(ChoreographyEventName.CARD_VERIFICATION_FAILED)) {
            log.warn("cancel booking with id {} because of reason {}", sagaEvent.getBookingId(), sagaEvent.getEventName());
            paymentService.rejectBooking(sagaEvent.getBookingId());
        }


    }

    @KafkaListener(topics = TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToFlightBookingsTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.FLIGHT_BOOKING_FAILED)) {
            log.warn("cancel booking with id {} because of reason {}", sagaEvent.getBookingId(), sagaEvent.getEventName());
            paymentService.rejectBooking(sagaEvent.getBookingId());
        }
    }


    @KafkaListener(topics = TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToHotelBookingsTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.HOTEL_BOOKING_FAILED)) {
            log.warn("cancel booking with id {} because of reason {}", sagaEvent.getBookingId(), sagaEvent.getEventName());
            paymentService.rejectBooking(sagaEvent.getBookingId());
        }
    }

}
