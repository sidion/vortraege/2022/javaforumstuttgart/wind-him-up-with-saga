package de.sidion.saga.flightbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import de.sidion.saga.flightbookingservice.entities.FlightBookingEntitiy;
import de.sidion.saga.flightbookingservice.persistence.FlightBookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class FlightBookingService {

    private final FlightBookingRepository flightBookingRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public FlightBookingService(FlightBookingRepository flightBookingRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.flightBookingRepository = flightBookingRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createNewFlightBooking(final BookingEvent bookingEvent) {

        final FlightBookingEntitiy flightBookingEntitiy = FlightBookingEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .outgoingFlightId(bookingEvent.getFlightEvent().getOutgoingFlightId())
                .outgoingFlightDate(bookingEvent.getFlightEvent().getOutgoingFlightDate())
                .returnFlightId(bookingEvent.getFlightEvent().getReturnFlightId())
                .returnFlightDate(bookingEvent.getFlightEvent().getReturnFlightDate())
                .bookingStatus("PENDING")
                .build();

        flightBookingRepository.save(flightBookingEntitiy);


    }

    public void handleUpdate(final Integer bookingId) {
        final FlightBookingEntitiy flightBookingEntitiy = flightBookingRepository.findByBookingId(bookingId);
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(bookingId);
        if (isValidFlightBooking(flightBookingEntitiy)) {
            builder.eventName(ChoreographyEventName.APPLY_HOTEL_BOOKING);
            flightBookingEntitiy.setBookingStatus("COMPLETED");
            flightBookingRepository.save(flightBookingEntitiy);
            sendSagaEvent(builder.build(), TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY);
        } else {
            builder.eventName(ChoreographyEventName.FLIGHT_BOOKING_FAILED);
            sendSagaEvent(builder.build(), TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY);
        }


    }

    private void sendSagaEvent(SagaEvent sagaEvent, String topic) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(topic, String.valueOf(sagaEvent.getBookingId()), sagaEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending sagaEvent {} to topic {}", sagaEvent, topic);
            }
        });
    }

    private boolean isValidFlightBooking(FlightBookingEntitiy flightBookingEntitiy) {

        // we only fly out with Lufthansa
        return flightBookingEntitiy.getOutgoingFlightId().startsWith("LH");
    }

    public void rejectBooking(Integer bookingId) {
        final FlightBookingEntitiy flightBookingEntitiyToDelete = flightBookingRepository.findByBookingId(bookingId);
        if (flightBookingEntitiyToDelete != null) {
            log.info("DELETE flightBookingEntitiy: {}", flightBookingEntitiyToDelete);
            flightBookingRepository.delete(flightBookingEntitiyToDelete);
        }
    }
}
