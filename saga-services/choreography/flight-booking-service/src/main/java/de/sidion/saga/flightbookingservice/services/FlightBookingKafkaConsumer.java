package de.sidion.saga.flightbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FlightBookingKafkaConsumer {

    private final FlightBookingService flightBookingService;

    public FlightBookingKafkaConsumer(FlightBookingService flightBookingService) {
        this.flightBookingService = flightBookingService;
    }

    @KafkaListener(topics = TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, containerFactory = "bookingEventKafkaListenerContainerBatchFactory")
    public void listenToBookingsTopic(@Payload final BookingEvent bookingEvent) {
        log.info("incomming bookingEvent from {}: {}", TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, bookingEvent);
        flightBookingService.createNewFlightBooking(bookingEvent);
    }

    @KafkaListener(topics = TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToFlightTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.APPLY_FLIGHT_BOOKING)) {
            flightBookingService.handleUpdate(sagaEvent.getBookingId());
        } else if (sagaEvent.getEventName().equals(ChoreographyEventName.FLIGHT_BOOKING_FAILED)) {
            flightBookingService.rejectBooking(sagaEvent.getBookingId());
        }

    }

    @KafkaListener(topics = TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToPaymentTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.DEBIT_CARD_FAILED)) {
            flightBookingService.rejectBooking(sagaEvent.getBookingId());
        }
    }

    @KafkaListener(topics = TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToHotelBookingsTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.HOTEL_BOOKING_FAILED)) {
            flightBookingService.rejectBooking(sagaEvent.getBookingId());
        }
    }

}
