package de.sidion.saga.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class BookingEvent {

    private Integer bookingId;
    private CustomerEvent customerEvent;
    private FlightEvent flightEvent;
    private HotelEvent hotelEvent;
    private PaymentEvent paymentEvent;
}
