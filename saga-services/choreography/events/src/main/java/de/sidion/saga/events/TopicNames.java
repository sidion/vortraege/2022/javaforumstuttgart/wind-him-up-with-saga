package de.sidion.saga.events;

public class TopicNames {

    public static final String TOPIC_BOOKINGS_CHOREOGRAPHY = "bookings-choreography";
    public static final String TOPIC_CONFIRMATION_CHOREOGRAPHY = "confirmation-choreography";
    public static final String TOPIC_PAYMENTS_CHOREOGRAPHY = "payments-choreography";
    public static final String TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY = "hotel-bookings-choreography";
    public static final String TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY = "flight-bookings-choreography";


    private TopicNames() {
        // only static members


    }
}
