package de.sidion.saga.holidaybookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import de.sidion.saga.holidaybookingservice.entities.BookingEntitiy;
import de.sidion.saga.holidaybookingservice.persistence.BookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
@Slf4j
public class HolidayBookingService {

    private final BookingRepository bookingRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;


    public HolidayBookingService(BookingRepository bookingRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.bookingRepository = bookingRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createBooking(final BookingEvent bookingEvent) {
        final BookingEntitiy bookingEntitiy = persistBooking(bookingEvent);
        bookingEvent.setBookingId(bookingEntitiy.getBookingId());
        sendBookingEventToServices(bookingEvent);
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
        sendSagaEventToPaymentService(bookingEvent.getBookingId());
    }

    private BookingEntitiy persistBooking(BookingEvent booking) {

        final BookingEntitiy bookingEntitiy = BookingEntitiy.builder()
                .bookingStatus("PENDING")
                .customerId(booking.getCustomerEvent().getCustomerId())
                .customerName(booking.getCustomerEvent().getName())
                .email(booking.getCustomerEvent().getEmail())
                .outgoingFlightId(booking.getFlightEvent().getOutgoingFlightId())
                .outgoingFlightDate(booking.getFlightEvent().getOutgoingFlightDate())
                .returnFlightId(booking.getFlightEvent().getReturnFlightId())
                .returnFlightDate(booking.getFlightEvent().getReturnFlightDate())
                .hotelId(booking.getHotelEvent().getHotelId())
                .hotelFromDate(booking.getHotelEvent().getFromDate())
                .hotelToDate(booking.getHotelEvent().getToDate())
                .price(booking.getPaymentEvent().getPrice())
                .cardNumber(booking.getPaymentEvent().getCardNumber())
                .build();

        return bookingRepository.save(bookingEntitiy);

    }

    public void rejectBooking(final Integer bookingId) {
        final BookingEntitiy bookingEntitiy = bookingRepository.findById(bookingId).get();
        bookingEntitiy.setBookingStatus("CANCELED");

        bookingRepository.save(bookingEntitiy);

    }

    public void finalizeBooking(final Integer bookingId) {
        final BookingEntitiy bookingEntitiy = bookingRepository.findById(bookingId).get();
        bookingEntitiy.setBookingStatus("COMPLETED");

        bookingRepository.save(bookingEntitiy);

    }

    private void sendBookingEventToServices(BookingEvent bookingEvent) {

        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY, String.valueOf(bookingEvent.getBookingId()), bookingEvent);

        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("send bookingEvent to topic: {}", TopicNames.TOPIC_BOOKINGS_CHOREOGRAPHY);
            }
        });


    }

    private void sendSagaEventToPaymentService(final Integer bookingId) {

        final SagaEvent sagaEvent = SagaEvent.builder().bookingId(bookingId).eventName(ChoreographyEventName.VERIFY_CARD).build();
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, String.valueOf(bookingId), sagaEvent);

        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("send sagaEvent to topic: {}", TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY);
            }
        });


    }

}
