package de.sidion.saga.holidaybookingservice.controller;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.holidaybookingservice.services.HolidayBookingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class BookingController {

    private final HolidayBookingService holidayBookingService;

    public BookingController(HolidayBookingService holidayBookingService) {
        this.holidayBookingService = holidayBookingService;
    }

    @PostMapping("/create")
    public void createBooking(@RequestBody final BookingEvent bookingEvent) {
        holidayBookingService.createBooking(bookingEvent);
    }

}
