package de.sidion.saga.holidaybookingservice.services;

import de.sidion.saga.events.ChoreographyEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.TopicNames;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class HolidayBookingKafkaConsumer {

    private final HolidayBookingService holidayBookingService;


    public HolidayBookingKafkaConsumer(HolidayBookingService holidayBookingService) {
        this.holidayBookingService = holidayBookingService;
    }


    @KafkaListener(topics = TopicNames.TOPIC_CONFIRMATION_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToConfirmationTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_CONFIRMATION_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.CONFIRMATION_APPLIED)) {
            log.info("finalize booking with id {}", sagaEvent.getBookingId());
            holidayBookingService.finalizeBooking(sagaEvent.getBookingId());
        }
    }

    @KafkaListener(topics = TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToPaymentTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_PAYMENTS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.CARD_VERIFICATION_FAILED)) {
            log.warn("cancel booking with id {} because of reason {}", sagaEvent.getBookingId(), sagaEvent.getEventName());
            holidayBookingService.rejectBooking(sagaEvent.getBookingId());
        } else if (sagaEvent.getEventName().equals(ChoreographyEventName.DEBIT_CARD_FAILED)) {
            log.warn("cancel booking with id {} because of reason {}", sagaEvent.getBookingId(), sagaEvent.getEventName());
            holidayBookingService.rejectBooking(sagaEvent.getBookingId());
        }
    }

    @KafkaListener(topics = TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToFlightBookingsTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_FLIGHT_BOOKINGS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.FLIGHT_BOOKING_FAILED)) {
            log.warn("cancel booking with id {} because of reason {}", sagaEvent.getBookingId(), sagaEvent.getEventName());
            holidayBookingService.rejectBooking(sagaEvent.getBookingId());
        }
    }

    @KafkaListener(topics = TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToHotelBookingsTopic(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent from {}: {}", TopicNames.TOPIC_HOTEL_BOOKINGS_CHOREOGRAPHY, sagaEvent);
        if (sagaEvent.getEventName().equals(ChoreographyEventName.HOTEL_BOOKING_FAILED)) {
            log.warn("cancel booking with id {} because of reason {}", sagaEvent.getBookingId(), sagaEvent.getEventName());
            holidayBookingService.rejectBooking(sagaEvent.getBookingId());
        }
    }

}
