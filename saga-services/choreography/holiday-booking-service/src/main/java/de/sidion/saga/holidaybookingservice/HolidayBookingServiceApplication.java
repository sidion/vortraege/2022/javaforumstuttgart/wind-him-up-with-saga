package de.sidion.saga.holidaybookingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolidayBookingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolidayBookingServiceApplication.class, args);
	}

}
