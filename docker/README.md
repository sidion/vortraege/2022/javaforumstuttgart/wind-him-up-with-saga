# docker

## Apache Kafka Container

```
docker-compose -f ./kafka/docker-compose.yml up -d
```


## MySQL Container für Choreography Based Saga

```
docker-compose -f ./mysql/choreography/docker-compose-holiday-booking-service.yml up -d
docker-compose -f ./mysql/choreography/docker-compose-flight-booking-service.yml up -d
docker-compose -f ./mysql/choreography/docker-compose-hotel-booking-service.yml up -d
docker-compose -f ./mysql/choreography/docker-compose-confirmation-service.yml up -d
docker-compose -f ./mysql/choreography/docker-compose-payment-service.yml up -d
```


## MySQL Container für Orchestration Based Saga

```
docker-compose -f ./mysql/orchestration/docker-compose-holiday-booking-service.yml up -d
docker-compose -f ./mysql/orchestration/docker-compose-flight-booking-service.yml up -d
docker-compose -f ./mysql/orchestration/docker-compose-hotel-booking-service.yml up -d
docker-compose -f ./mysql/orchestration/docker-compose-confirmation-service.yml up -d
docker-compose -f ./mysql/orchestration/docker-compose-payment-service.yml up -d
```



