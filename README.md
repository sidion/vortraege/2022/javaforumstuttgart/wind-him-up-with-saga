# Wind Him Up - Mit Saga verteilte Transaktionen in einer Kafka-Architektur verwalten

## Abstract
In modernen Microservice-Architekturen mit eigener Datenhaltung besteht immer wieder das Problem der Datenkonsistenz bei Fehlerfällen innerhalb verteilter Transaktionen. Anstatt Distributed Transactions zu verwenden wird mittlerweile hauptsächlich auf die Verwendung des Saga-Patterns gesetzt. In diesem Vortrag möchte ich gerne auf die Problemstellungen und Lösungen dazu im theoretischen wie auch im praktischen Beispiel darauf eingehen.

## Speaker Biographie
Thomas Müller ist Senior Software Developer der sidion GmbH aus Stuttgart. Er ist seit 2003 passionierter Java-Entwickler und dort auch in sämtlichen Backend- und Middleware-Systemen unterwegs. Seine beruflichen Lieblingsspielplätze sind Apache Kafka, Hazelcast sowie Spring-Boot-Applikationen. Und wenn dann mal noch etwas freie Zeit übrig bleibt, ist das Raspberry-PI-Umfeld eine nette Abwechslung.

Kontakt über thomas.mueller@sidion.de


## Code
Im Unterverzeichniss [saga-services](saga-services/) findest Du jeweile eine Implementierung für die Choreography-Based-Sage und die Orchestration-Based-Saga.

Im Unterverzeichniss [docker](docker/) findest Du für Apache Kafka sowie für alle Datenbank-Inzstanzen docker-compose-Scripte um die Container zu starten.

Im Unterverzeichniss [akhq](akhq/) findest Du einen AKHQ um dir die Topics anzeigen zu lassen.


